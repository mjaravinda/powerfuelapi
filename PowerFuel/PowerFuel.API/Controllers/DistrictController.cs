﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictController : ControllerBase
    {
        #region Private Feilds
        private readonly IGenaricBL<District> _bl;
        #endregion

        #region Constructor
        public DistrictController(IGenaricBL<District> bl)
        {
            _bl = bl;
        }
        #endregion

       // [Authorize]
        [HttpGet]
        [Route("All")]

        public async Task<IEnumerable<District>> GetAll()
        {
            try
            {
                return await _bl.GetAll();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

       // [Authorize]
        [HttpGet]
        [Route("ById")]

        public async Task<District> GetById(long? id)
        {
            try
            {
                return await _bl.GetById(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Save")]
        public async Task<IActionResult> Save([FromBody] District district)
        {
            try
            {
                var r = await _bl.Save(district);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok();
                }

                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Delete")]

        public async Task<IActionResult> Delete(long? id)
        {
            try
            {
                var r = await _bl.Delete(id);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok(r.Response);
                }
                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
