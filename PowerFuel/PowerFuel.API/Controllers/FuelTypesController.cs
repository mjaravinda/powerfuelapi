﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuelTypesController : ControllerBase
    {
        #region Private Feilds
        private readonly IGenaricBL<FuelType> _bl;
        #endregion

        #region Constructor
        public FuelTypesController(IGenaricBL<FuelType> bl)
        {
            _bl = bl;
        }
        #endregion

     //   [Authorize]
        [HttpGet]
        [Route("All")]

        public async Task<IEnumerable<FuelType>> GetAll()
        {
            try
            {
                return await _bl.GetAll();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

     //   [Authorize]
        [HttpGet]
        [Route("ById")]

        public async Task<FuelType> GetById(long? id)
        {
            try
            {
                return await _bl.GetById(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Save")]
        public async Task<IActionResult> Save([FromBody] FuelType fuelType)
        {
            try
            {
                var r = await _bl.Save(fuelType);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok();
                }

                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Delete")]

        public async Task<IActionResult> Delete(long? id)
        {
            try
            {
                var r = await _bl.Delete(id);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok(r.Response);
                }
                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}