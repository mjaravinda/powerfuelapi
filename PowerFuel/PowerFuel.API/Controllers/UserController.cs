﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PowerFuel.BL;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Private Feilds
        private readonly IGenaricBL<User> _bl;
        private readonly IUserBL _user_bl;
        private readonly IConfiguration _config;
        #endregion

        #region Constructor
        public UserController(IGenaricBL<User> bl, IUserBL user_bl, IConfiguration config)
        {
            _bl = bl;
            _user_bl = user_bl;
            _config = config;
        }
        #endregion
        [HttpGet]
        [Route("All")]
        public async Task<IEnumerable<User>> GetAll()
        {
            try
            {
                return await _bl.GetAll();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Save")]

        public async Task<IActionResult> Save([FromBody] User user)
        {
            try
            {
                var r = await _bl.Save(user);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok();
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("login")]

        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            try
            {
                var r = await _user_bl.Login(login);
                if (r.Status == Common.ActionStatus.Success)
                {
                    var token = BuildToken(r.Response);
                    r.Response.Token = token;
                    return Ok(r.Response);
                }
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Delete")]

        public async Task<IActionResult> Delete(long? id)
        {
            try
            {
                var r = await _bl.Delete(id);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok(r.Response);
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private string BuildToken(User login)
        {
            try
            {
                var claims = new[] {

                new Claim(ClaimTypes.Name, login.FirstName),
                new Claim("userId",(login.Id.ToString())),
                new Claim("email",login.Email),
                new Claim("contactNo",login.ContactNo),
            };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                  _config["Jwt:Issuer"],
                  expires: DateTime.Now.AddDays(7),
                  claims: claims,
                  signingCredentials: creds);

                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
