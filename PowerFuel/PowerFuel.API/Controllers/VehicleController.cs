﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        #region Private Feilds
        private readonly IGenaricBL<Vehicle> _bl;
        private readonly IVehicleBL _vbl;
        private readonly IConfiguration _config;
        #endregion

        #region Constructor
        public VehicleController(IGenaricBL<Vehicle> bl, IVehicleBL vbl, IConfiguration config)
        {
            _bl = bl;
            _vbl = vbl;
            _config = config;
        }
        #endregion

        [Authorize]
        [HttpGet]
        [Route("All")]

        public async Task<IEnumerable<Vehicle>> GetAll()
        {
            try
            {
                return await _bl.GetAll();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("AllByCustomer")]

        public async Task<IEnumerable<Vehicle>> GetAllByCustomer(long? custoemrId)
        {
            try
            {
                return await _vbl.GetAll(custoemrId);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Save")]

        public async Task<IActionResult> Save([FromBody] Vehicle vehicle)
        {
            try
            {
                var r = await _bl.Save(vehicle);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok();
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Delete")]

        public async Task<IActionResult> Delete(long? id)
        {
            try
            {
                var r = await _bl.Delete(id);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok(r.Response);
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpPost]
        [Route("ChangeStatusOfAVehicle")]

        public async Task<IActionResult> ChangeStatusOfAVehicle(long? vehicleId, VehicleStatus status)
        {
            try
            {
                var r = await _vbl.ChangeStatus(vehicleId, status);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok(r.Response);
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ChangeStatusOfAVehicles")]

        public async Task<IActionResult> ChangeStatusOfAVehicles([FromBody] VehiceChangeRequest model)
        {
            try
            {
                var r = await _vbl.ChangeStatus(model.VehicleIds, model.VehicleStatus);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok(r.Response);
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [Route("UpdateVehiclesPeriodQTA")]

        public async Task<IActionResult> UpdateVehiclesPeriodQTA([FromBody] List<Vehicle> vehicles)
        {
            try
            {
                var r = await _vbl.UpdateAlVehicles(vehicles);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok(r.Response);
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

    public class VehiceChangeRequest
    {
        public long?[] VehicleIds { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
    }
}
