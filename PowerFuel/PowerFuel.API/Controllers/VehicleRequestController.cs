﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleRequestController : ControllerBase
    {
        private readonly IVehicleRequestBL _vbl;
        private readonly IConfiguration _config;


  
        public VehicleRequestController(IVehicleRequestBL vbl, IConfiguration config)
        {        
            _vbl = vbl;      
            _config= config;
        }

        [HttpPost]
        [Route("UpdateStatus")]

        public async Task<IActionResult> UpdateStatus([FromBody] VehicleQuataRequest req)
        {
            try
            {
                var r = await _vbl.UpdateStatus(req);
                if (r.Status == Common.ActionStatus.Success)
                    return Ok();
                else
                    return BadRequest(r.Message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("MakeRequest")]

        public async Task<IActionResult> MakeRequest([FromBody] VehicleQuataRequest req)
        {
            try
            {
                var r = await _vbl.MakeRequest(req);
                return Ok(r);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
