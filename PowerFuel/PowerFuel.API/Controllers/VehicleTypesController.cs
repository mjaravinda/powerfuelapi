﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;

namespace PowerFuel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleTypesController : ControllerBase
    {
        #region Private Feilds
        private readonly IGenaricBL<VehicleType> _bl;
        #endregion

        #region Constructor
        public VehicleTypesController(IGenaricBL<VehicleType> bl)
        {
            _bl = bl;
        }
        #endregion

      //  [Authorize]
        [HttpGet]
        [Route("All")]

        public async Task<IEnumerable<VehicleType>> GetAll()
        {
            try
            {
                return await _bl.GetAll();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

      //  [Authorize]
        [HttpGet]
        [Route("ById")]

        public async Task<VehicleType> GetById(long? id)
        {
            try
            {
                return await _bl.GetById(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Save")]
        public async Task<IActionResult> Save([FromBody] VehicleType vehicleType)
        {
            try
            {
                var r = await _bl.Save(vehicleType);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok();
                }

                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Delete")]

        public async Task<IActionResult> Delete(long? id)
        {
            try
            {
                var r = await _bl.Delete(id);
                if (r.Status == Common.ActionStatus.Success)
                {
                    return Ok(r.Response);
                }
                else
                {
                    return BadRequest(r.Message);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}