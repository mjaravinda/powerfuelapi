using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PowerFuel.BL;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using PowerFuel.DA;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

var securityScheme = new OpenApiSecurityScheme()
{
    Name = "Authorization",
    Type = SecuritySchemeType.ApiKey,
    Scheme = "Bearer",
    BearerFormat = "JWT",
    In = ParameterLocation.Header,
    Description = "JSON Web Token based security",
};

var securityReq = new OpenApiSecurityRequirement()
{
    {
        new OpenApiSecurityScheme
        {
            Reference = new OpenApiReference
            {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
            }
        },
        new string[] {}
    }
};

var contact = new OpenApiContact()
{
    Name = "Janaka Aravinda",
    Email = "mjaravinda@gmail.com",

};



var info = new OpenApiInfo()
{
    Version = "v1",
    Title = "Power Fuel API",
    Description = "API for manage PowerFuel",
    //   TermsOfService = new Uri("http://www.example.com"),
    Contact = contact,

};





builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
 options.TokenValidationParameters = new TokenValidationParameters
 {
     ValidateIssuer = true,
     ValidateAudience = true,
     ValidateLifetime = true,
     ValidateIssuerSigningKey = true,
     ValidIssuer = builder.Configuration["Jwt:Issuer"],
     ValidAudience = builder.Configuration["Jwt:Issuer"],
     IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
 };
});
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder => builder.AllowAnyOrigin()
          .AllowAnyMethod()
          .AllowAnyHeader()
          .Build());
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o =>
{
    o.SwaggerDoc("v1", info);
    o.AddSecurityDefinition("Bearer", securityScheme);
    o.AddSecurityRequirement(securityReq);
});



builder.Services.AddScoped<IGenaricDA<User>, UserDA>();
builder.Services.AddScoped<IUserDA, UserDA>();

builder.Services.AddScoped<IGenaricBL<User>, UserBL>();
builder.Services.AddScoped<IUserBL, UserBL>();


builder.Services.AddScoped<IGenaricDA<Customer>, CustomerDA>();
builder.Services.AddScoped<ICustomerDA, CustomerDA>();

builder.Services.AddScoped<IGenaricBL<Customer>, CustomerBL>();
builder.Services.AddScoped<ICustomerBL, CustomerBL>();


builder.Services.AddScoped<IGenaricDA<Vehicle>, VehicleDA>();
builder.Services.AddScoped<IVehicleDA, VehicleDA>();

builder.Services.AddScoped<IGenaricBL<Vehicle>, VehicleBL>();
builder.Services.AddScoped<IVehicleBL, VehicleBL>();

builder.Services.AddScoped<IVehicleRequestBL, VehicleQuataRequestBL>();
builder.Services.AddScoped<IVehicleRequestDA, VehicleRequestDA>();

builder.Services.AddScoped<IGenaricDA<UserType>, UserTypesDA>();
builder.Services.AddScoped<IGenaricDA<VehicleType>, VehicleTypesDA>();
builder.Services.AddScoped<IGenaricDA<District>, DistrictDA>();
builder.Services.AddScoped<IGenaricDA<FuelType>, FuelTypesDA>();
builder.Services.AddScoped<IGenaricDA<FuelStation>, FuelStationDA>();
builder.Services.AddScoped<IGenaricDA<FuelStationRequest>, FuelStationRequestsDA>();

builder.Services.AddScoped<IGenaricBL<UserType>, UserTypesBL>();
builder.Services.AddScoped<IGenaricBL<VehicleType>, VehicleTypesBL>();
builder.Services.AddScoped<IGenaricBL<District>, DistrictBL>();
builder.Services.AddScoped<IGenaricBL<FuelType>, FuelTypesBL>();
builder.Services.AddScoped<IGenaricBL<FuelStation>, FuelStationBL>();
builder.Services.AddScoped<IGenaricBL<FuelStationRequest>, FuelStationRequestsBL>();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  
}
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();


app.Run();
