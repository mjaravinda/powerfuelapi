﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class CustomerBL : IGenaricBL<Customer>, ICustomerBL
    {
        private IGenaricDA<Customer> da;
        private ICustomerDA cda;
        public CustomerBL(IGenaricDA<Customer> da, ICustomerDA cda)
        {
            this.da = da;
            this.cda = cda;
        }

        public Task<ActionResult<Customer>> Delete(long? id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
          return  await da.GetAll();
        }

        public Task<Customer> GetById(long? id)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResult<Customer>> Login(LoginModel model)
        {
            var user = await cda.GetByEmailOrPhoneNumber(model.UserName);
            if (user != null)
            {
                var salt = Convert.FromBase64String(user.Salt);
                var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                               password: model.Password,
                               salt: salt,
                               prf: KeyDerivationPrf.HMACSHA1,
                               iterationCount: 10000,
                               numBytesRequested: 256 / 8)); ;

                if (hashed.Equals(user.Password))
                {
                    user.Password = "";
                    user.Salt = "";
                    return new ActionResult<Customer> { Status = ActionStatus.Success, Response = user };
                }
                else
                    return new ActionResult<Customer> { Status = ActionStatus.Error, Message = "Password is invalied" };
            }
            else
            {
                return new ActionResult<Customer> { Status = ActionStatus.Error, Message = "User not found" };
            }
        }

        public async Task<ActionResult<Customer>> Save(Customer t)
        {

            try
            {
                if (!string.IsNullOrEmpty(t.Password))
                {
                    t.CleanPassword = t.Password;
                    GenaratePasswordHash(t);
                }

                return await da.Save(t);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void GenaratePasswordHash(Customer model)
        {
            try
            {
                byte[] salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }

                model.Salt = Convert.ToBase64String(salt);
                model.Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                               password: model.Password,
                               salt: salt,
                               prf: KeyDerivationPrf.HMACSHA1,
                               iterationCount: 10000,
                               numBytesRequested: 256 / 8));
            }
            catch (Exception ex)
            {
               
                throw;
            }

        }


        public Task<ActionResult<Customer>> VarifyOTP(long? userId, string OTP)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<Customer>> VerifyOTP(LoginModel model)
        {
            throw new NotImplementedException();
        }
    }
}
