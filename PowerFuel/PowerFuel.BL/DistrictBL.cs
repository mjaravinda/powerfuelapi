﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class DistrictBL : IGenaricBL<District>
    {
        private IGenaricDA<District> da;
        public DistrictBL(IGenaricDA<District> da) 
        { 
            this.da = da;
        }
        public async Task<ActionResult<District>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<District>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<District> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<District>> Save(District t)
        {
            return await da.Save(t);
        }
    }
}
