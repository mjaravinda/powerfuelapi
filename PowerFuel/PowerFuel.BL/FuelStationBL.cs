﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class FuelStationBL : IGenaricBL<FuelStation>
    {
        private IGenaricDA<FuelStation> da;
        public FuelStationBL(IGenaricDA<FuelStation> da)
        {
            this.da = da;
        }

        public async Task<ActionResult<FuelStation>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<FuelStation>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<FuelStation> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<FuelStation>> Save(FuelStation t)
        {
            return await da.Save(t);
        }
    }
}
