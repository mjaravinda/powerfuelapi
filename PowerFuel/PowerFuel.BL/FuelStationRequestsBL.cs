﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class FuelStationRequestsBL : IGenaricBL<FuelStationRequest>
    {
        private IGenaricDA<FuelStationRequest> da;
        public FuelStationRequestsBL(IGenaricDA<FuelStationRequest> da) 
        {
            this.da = da;
        }
        public async Task<ActionResult<FuelStationRequest>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<FuelStationRequest>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<FuelStationRequest> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<FuelStationRequest>> Save(FuelStationRequest t)
        {
            return await da.Save(t);
        }
    }
}
