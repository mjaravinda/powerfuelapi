﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class FuelTypesBL : IGenaricBL<FuelType>
    {
        private IGenaricDA<FuelType> da;
        public FuelTypesBL(IGenaricDA<FuelType> da)
        {
            this.da = da;
        }
        public async Task<ActionResult<FuelType>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<FuelType>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<FuelType> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<FuelType>> Save(FuelType t)
        {
            return await da.Save(t);
        }
    }
}
