﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class UserBL : IGenaricBL<User>,IUserBL
    {
        private IGenaricDA<User> da;
        private IUserDA uda;
        public UserBL( IGenaricDA<User> da) 
        { 
            this.da = da;
        }
        public async Task<ActionResult<User>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<User> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<User>> Login(LoginModel model)
        {
            var user = await uda.GetByEmailOrPhoneNumber(model.UserName);
            if (user != null)
            {
                var salt = Convert.FromBase64String(user.Salt);
                var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                               password: model.Password,
                               salt: salt,
                               prf: KeyDerivationPrf.HMACSHA1,
                               iterationCount: 10000,
                               numBytesRequested: 256 / 8)); ;

                if (hashed.Equals(user.Password))
                {
                    user.Password = "";
                    user.Salt = "";
                    return new ActionResult<User> { Status = ActionStatus.Success, Response = user };
                }
                else
                    return new ActionResult<User> { Status = ActionStatus.Error, Message = "Password is invalied" };
            }
            else
            {
                return new ActionResult<User> { Status = ActionStatus.Error, Message = "User not found" };
            }
        }

        public async Task<ActionResult<User>> Save(User t)
        {
            try
            {
                if (!string.IsNullOrEmpty(t.Password))
                {
                    GenaratePasswordHash(t);
                }

                return await da.Save(t);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void GenaratePasswordHash(User model)
        {
            try
            {
                byte[] salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }

                model.Salt = Convert.ToBase64String(salt);
                model.Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                               password: model.Password,
                               salt: salt,
                               prf: KeyDerivationPrf.HMACSHA1,
                               iterationCount: 10000,
                               numBytesRequested: 256 / 8));
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}
