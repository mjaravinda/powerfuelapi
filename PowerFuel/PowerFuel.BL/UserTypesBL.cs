﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class UserTypesBL : IGenaricBL<UserType>
    {
        private IGenaricDA<UserType> da;
        public UserTypesBL(IGenaricDA<UserType> da) 
        {
            this.da = da;
        }
        public async Task<ActionResult<UserType>> Delete(long? id)
        {
           return await da.Delete(id);
        }

        public async Task<IEnumerable<UserType>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<UserType> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<UserType>> Save(UserType t)
        {
            return await da.Save(t);
        }
    }
}
