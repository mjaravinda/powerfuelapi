﻿using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class VehicleBL : IGenaricBL<Vehicle>, IVehicleBL
    {
        private IGenaricDA<Vehicle> da;
        private IVehicleDA cda;
        public VehicleBL(IGenaricDA<Vehicle> da, IVehicleDA cda)
        {
            this.da = da;
            this.cda = cda;
        }

        public async Task<ActionResult<Vehicle>> ChangeStatus(long? VehicleId, VehicleStatus status)
        {
            return await cda.ChangeStatus(VehicleId, status);
        }

        public async Task<ActionResult<Vehicle>> ChangeStatus(long?[] VehicleId, VehicleStatus status)
        {
            return await cda.ChangeStatus(VehicleId, status);
        }

        public async Task<ActionResult<Vehicle>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<Vehicle>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<IEnumerable<Vehicle>> GetAll(long? customerId)
        {
            return await cda.GetAll(customerId);
        }

        public async Task<Vehicle> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<Vehicle>> Save(Vehicle t)
        {
           return await da.Save(t);
        }

        public async Task<ActionResult<Vehicle>> UpdateAlVehicles(List<Vehicle> vehicle)
        {
            return await cda.UpdateAlVehicles(vehicle);
        }
    }
}
