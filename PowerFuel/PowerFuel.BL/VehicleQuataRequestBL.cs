﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class VehicleQuataRequestBL : IVehicleRequestBL
    {
        IVehicleRequestDA _da;
        public VehicleQuataRequestBL(IVehicleRequestDA da)
        {
            _da = da;
        }
        public async Task<ActionResult<VehicleQuataRequest>> MakeRequest(VehicleQuataRequest request)
        {
            var vehicle =await _da.GetVehicle(request);
            var fuelStation =await _da.GetFuelStaion(request);
            var isQuataAvailable = await _da.IsQuataAvailable(request);
            if (!isQuataAvailable)
                return new ActionResult<VehicleQuataRequest> { Status = Common.ActionStatus.InCompleted, Message = "Period Quata Exceeded" };
            
            var dailyPlans =await CreatePlan(fuelStation.DurationPerVerhicle ?? 2, request.RequestedDateTime.Value, request.FuelStationId.Value, request.FuelTypeId.Value);
            if (dailyPlans == null || dailyPlans.Count == 0)
            {
                return new ActionResult<VehicleQuataRequest> { Status = Common.ActionStatus.InCompleted, Message = "No Time slot avilable.please try again with new time" };

            }
            else
            {
                var maxPlan = dailyPlans.FirstOrDefault(w => w.PumpedTime >= request.RequestedDateTime);
                var minPlan = dailyPlans.FirstOrDefault(w => w.PumpedTime <= request.RequestedDateTime);
                if (maxPlan == null)
                    request.RequestedDateTime = minPlan.PumpedTime;
                else if (minPlan == null)
                    request.RequestedDateTime = maxPlan.PumpedTime;
                else
                {
                    if ((maxPlan.PumpedTime - request.RequestedDateTime.Value).TotalMinutes < (request.RequestedDateTime.Value - minPlan.PumpedTime).TotalMinutes) 
                    {
                        request.RequestedDateTime = maxPlan.PumpedTime;
                    }
                    else
                    {
                        request.RequestedDateTime = minPlan.PumpedTime;
                    }
                }
                request.RequestStatus = Common.RequestStatus.Confirmed;
                return await _da.Save(request);
            }




        }

        public async Task<ActionResult<VehicleQuataRequest>> UpdateStatus(VehicleQuataRequest request)
        {
          return  await _da.Save(request);
        }

        private async Task<List<FuelStationDailyPlan>> CreatePlan(int noOfMins, DateTime reqdate,long FuelStationId, int fuelTypeId)
        {
             var list = new List<FuelStationDailyPlan>();
            var date = reqdate.Date;
            var minTime =  reqdate.AddHours(3);
            var maxtime = reqdate.AddHours(-3);

            var otherrequests =await _da.GetFuelRequestList(minTime,maxtime,FuelStationId,fuelTypeId);
            if (minTime < date)
                minTime = date;

            if (maxtime > date.AddDays(1).AddMinutes(-2))
                maxtime = date.AddDays(1).AddMinutes(-2);

            var maxSlot = date.AddDays(1).AddMinutes(-1 * noOfMins);
            while (date <= maxSlot ) 
            {
                date = date.AddMinutes(noOfMins);
                if (otherrequests.Any(a => a.RequestedDateTime.Value.ToString("yyyy-MM-dd HH:mm") == date.ToString("yyyy-MM-dd HH:mm")))
                    continue;


                if (minTime <= date && date < maxtime)
                    list.Add(new FuelStationDailyPlan { PumpedTime = date });
                

            }

            return list;


        }
    }
}
