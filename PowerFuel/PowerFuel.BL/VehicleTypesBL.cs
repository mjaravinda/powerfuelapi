﻿using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.BL
{
    public class VehicleTypesBL : IGenaricBL<VehicleType>
    {
        private IGenaricDA<VehicleType> da;
        public VehicleTypesBL(IGenaricDA<VehicleType> da) 
        { 
            this.da = da;
        } 
        public async Task<ActionResult<VehicleType>> Delete(long? id)
        {
            return await da.Delete(id);
        }

        public async Task<IEnumerable<VehicleType>> GetAll()
        {
            return await da.GetAll();
        }

        public async Task<VehicleType> GetById(long? id)
        {
            return await da.GetById(id);
        }

        public async Task<ActionResult<VehicleType>> Save(VehicleType t)
        {
            return await da.Save(t);
        }
    }
}
