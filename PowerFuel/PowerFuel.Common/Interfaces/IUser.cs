﻿using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Interfaces
{
    public interface IUserBL
    {
        Task<ActionResult<User>> Login(LoginModel model);
    }
    public interface IUserDA
    {
        Task<User?> GetByEmailOrPhoneNumber(string content);

    }
}
