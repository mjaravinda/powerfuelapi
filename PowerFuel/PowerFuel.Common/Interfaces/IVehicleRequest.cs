﻿using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Interfaces
{
    public interface IVehicleRequestDA
    {
        Task<FuelStation> GetFuelStaion(VehicleQuataRequest request);
        Task<Vehicle> GetVehicle(VehicleQuataRequest request);
        Task<bool> IsQuataAvailable(VehicleQuataRequest request);
        Task<List<VehicleQuataRequest>> GetFuelRequestList(DateTime start, DateTime end, long? fuelStationId, int? fuelTypeId);
        public Task<ActionResult<VehicleQuataRequest>> Save(VehicleQuataRequest request);
 
    }

    public interface IVehicleRequestBL
    {
        public Task<ActionResult<VehicleQuataRequest>> MakeRequest(VehicleQuataRequest request);
        public Task<ActionResult<VehicleQuataRequest>> UpdateStatus(VehicleQuataRequest request);
    }
}
