﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class ActionResult<T>
    {
        public string? Message { get; set; }
        public ActionStatus Status { get; set; }
        public T? Response { get; set; }
    }
}
