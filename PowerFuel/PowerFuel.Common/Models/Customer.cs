﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class Customer: GenaricModel
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? ContactNo { get; set; }
        public string? Password { get; set; }
        public string? OTP { get; set; }
        public string? Salt { get; set; }
        public DateTime? OTPExpireDate { get; set; }

        [NotMapped]
        public string? CleanPassword { get; set; }
        [NotMapped]
        public string? Token { get; set; }
    }
}
