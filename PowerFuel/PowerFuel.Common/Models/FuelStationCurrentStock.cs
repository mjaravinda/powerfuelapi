﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class FuelStationCurrentStock:GenaricModel
    {
        public int? FuelTypeId { get; set; }
        [ForeignKey("FuelTypeId")]
        public virtual FuelType? FuelType { get; set; }

        public long? FuelStationId { get; set; }
        [ForeignKey("FuelStationId")]
        public virtual FuelStation? FuelStation { get; set; }

        public float Qty { get; set; }
    }
}
