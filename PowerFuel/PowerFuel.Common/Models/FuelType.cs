﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class FuelType
    {
        [Key]   
        public int Id { get; set; }
        public string? Name { get; set; }
        public float? ROLOfStation { get; set; }
    }
}
