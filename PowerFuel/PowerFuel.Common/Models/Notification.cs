﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public  class Notification:GenaricModel
    {
        public string? Description { get; set; }
        public long? VehicleId { get; set; }
        [ForeignKey("VehicleId")]
        public virtual Vehicle? Vehicle { get; set; }

        public long? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer? Customer { get; set; }

        public long? FuelStationId { get; set; }
        [ForeignKey("FuelStationId")]
        public virtual FuelStation? FuelStation { get; set; }

        public NotificationStatus? NotificationStatus { get; set; }

    }
}
