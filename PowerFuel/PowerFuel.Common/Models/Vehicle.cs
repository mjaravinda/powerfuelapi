﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class Vehicle:GenaricModel
    {
        public string? PlateNo { get; set; }
        public string? ChassyNo { get; set; }

        public int? FuelTypeId { get; set; }
        [ForeignKey("FuelTypeId")]
        public virtual FuelType? FuelType { get; set; }

        public int? VehicleTypeId { get; set; }
        [ForeignKey("VehicleTypeId")]
        public virtual VehicleType? VehicleType { get; set; }

        public long? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer? Customer { get; set; }

        public float? UsedQTA { get; set; }
        public float? PeriodQTA  { get; set; }
        public float? CurrentPeriodStartTime  { get; set; }


        public VehicleStatus? VehicleStatus { get; set; }
   


    }
}
