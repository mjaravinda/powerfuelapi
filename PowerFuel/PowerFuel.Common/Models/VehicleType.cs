﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class VehicleType
    {
        [Key]      
        public int Id { get; set; }
        public string? Name { get; set; }
        public float? PeriodAllocatedQTA { get; set; }
        public Period? Period { get; set; }
        public int NoOfPeriods{ get; set; }
    }
}
