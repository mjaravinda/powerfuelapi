﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class CustomerDA : IGenaricDA<Customer>, ICustomerDA
    {
        public async Task<ActionResult<Customer>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var customer = await db.Customers.Where(w => w.Id == id).FirstOrDefaultAsync();
                    customer.State = Common.State.Deleted;
                    return new ActionResult<Customer> { Status = Common.ActionStatus.Success };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<Customer> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var customers = await db.Customers.Where(w => w.State == Common.State.Active).ToListAsync();
                    return customers;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Customer?> GetByEmailOrPhoneNumber(string content)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var customer = await db.Customers.Where(w =>  w.Email == content || w.ContactNo == content).FirstOrDefaultAsync();
                    return customer;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Customer?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var customer = await db.Customers.Where(w => w.Id == id ).FirstOrDefaultAsync();
                    return customer;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

      

        public async Task<ActionResult<Customer>> Save(Customer t)
        {
            try
            {
               
                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        t.State = Common.State.Active;
                        t.Created = DateTime.UtcNow;
                        t.LastUpdated = DateTime.UtcNow;
                        db.Customers.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else 
                    {
                        var customer = await db.Customers.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        customer.Email = t.Email;
                        customer.OTPExpireDate = t.OTPExpireDate;
                        customer.OTP = t.OTP;
                        customer.LastUpdated = DateTime.UtcNow;
                        customer.ContactNo = t.ContactNo;
                        customer.FirstName = t.FirstName;
                        customer.LastName = t.LastName;
                        if (!string.IsNullOrEmpty(customer.Password))
                        {
                            customer.Password = t.Password;
                            customer.Salt = t.Salt;
                        }
                        await db.SaveChangesAsync();
                        t.Id = customer.Id;
                    }
           
                         
                  
                    return new ActionResult<Customer> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<Customer> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

      
    }
}
