﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class DistrictDA : IGenaricDA<District>
    {
        public async Task<ActionResult<District>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var district = await db.Districts.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (district != null)
                    {
                        db.Districts.Remove(district);
                        await db.SaveChangesAsync();
                        return new ActionResult<District> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<District> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<District> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<District>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var districts = await db.Districts.ToListAsync();
                    return districts;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<District?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var district = await db.Districts.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return district;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<District>> Save(District t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        db.Districts.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var district = await db.Districts.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if(district != null)
                        {
                            district.Name = t.Name;
                            district.PopulationDensity = t.PopulationDensity;
                            await db.SaveChangesAsync();
                        }
                       

                    }
                    return new ActionResult<District> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<District> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}
