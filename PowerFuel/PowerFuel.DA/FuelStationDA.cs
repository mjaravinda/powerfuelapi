﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class FuelStationDA : IGenaricDA<FuelStation>
    {
        public async Task<ActionResult<FuelStation>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStation = await db.FuelStations.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (fuelStation != null)
                    {
                        fuelStation.State = Common.State.Deleted;
                        await db.SaveChangesAsync();
                        return new ActionResult<FuelStation> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<FuelStation> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelStation> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<FuelStation>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStations = await db.FuelStations.Where(w => w.State == Common.State.Active).ToListAsync();
                    return fuelStations;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FuelStation?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStation = await db.FuelStations.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return fuelStation;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<FuelStation>> Save(FuelStation t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        t.State = Common.State.Active;
                        t.Created = DateTime.UtcNow;
                        t.LastUpdated = DateTime.UtcNow;
                        db.FuelStations.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var fuelStation = await db.FuelStations.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (fuelStation != null)
                        {
                            fuelStation.Name = t.Name;
                            fuelStation.Email = t.Email;
                            fuelStation.Address = t.Address;
                            fuelStation.ContactPerson = t.ContactPerson;
                            fuelStation.DistrictId = t.DistrictId;
                            fuelStation.DurationPerVerhicle = t.DurationPerVerhicle;
                            fuelStation.LastUpdated = DateTime.UtcNow;
                            await db.SaveChangesAsync();
                        }
                      
                    }



                    return new ActionResult<FuelStation> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelStation> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}
