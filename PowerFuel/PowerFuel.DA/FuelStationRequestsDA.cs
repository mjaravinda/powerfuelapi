﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class FuelStationRequestsDA : IGenaricDA<FuelStationRequest>
    {
        public async Task<ActionResult<FuelStationRequest>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStationRequest = await db.Customers.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (fuelStationRequest != null)
                    {
                        fuelStationRequest.State = Common.State.Deleted;
                        return new ActionResult<FuelStationRequest> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<FuelStationRequest> { Status = Common.ActionStatus.Error };
                    }

                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelStationRequest> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<FuelStationRequest>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStationRequests = await db.FuelStationRequests.ToListAsync();
                    return fuelStationRequests;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FuelStationRequest?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelStationRequest = await db.FuelStationRequests.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return fuelStationRequest;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<FuelStationRequest>> Save(FuelStationRequest t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        t.State = Common.State.Active;
                        t.Created = DateTime.UtcNow;
                        t.LastUpdated = DateTime.UtcNow;
                        db.FuelStationRequests.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var fuelStationRequest = await db.FuelStationRequests.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (fuelStationRequest != null)
                        {
                            fuelStationRequest.OrderNumber = t.OrderNumber;
                            fuelStationRequest.FuelStationId= t.FuelStationId;
                            fuelStationRequest.FuelTypeId= t.FuelTypeId;
                            fuelStationRequest.Qty= t.Qty;
                            fuelStationRequest.ReceivedDateTime= t.ReceivedDateTime;
                            fuelStationRequest.RequestedDateTime= t.RequestedDateTime;
                            fuelStationRequest.PumpedUserId= t.PumpedUserId;
                            fuelStationRequest.RequestStatus= t.RequestStatus;
                            fuelStationRequest.LastUpdated= DateTime.UtcNow;
                            await db.SaveChangesAsync();
                        }

                    }
                    return new ActionResult<FuelStationRequest> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelStationRequest> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}
