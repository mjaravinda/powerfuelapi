﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class FuelTypesDA : IGenaricDA<FuelType>
    {
        public async Task<ActionResult<FuelType>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelType = await db.FuelTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (fuelType != null)
                    {
                        db.FuelTypes.Remove(fuelType);
                        await db.SaveChangesAsync();
                        return new ActionResult<FuelType> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<FuelType> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<FuelType>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelTypes = await db.FuelTypes.ToListAsync();
                    return fuelTypes;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FuelType?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var fuelType = await db.FuelTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return fuelType;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<FuelType>> Save(FuelType t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        db.FuelTypes.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var fuelType = await db.FuelTypes.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (fuelType != null)
                        {
                            fuelType.Name = t.Name;
                            fuelType.ROLOfStation = t.ROLOfStation;
                            await db.SaveChangesAsync();
                        }
                       
                    }
                    return new ActionResult<FuelType> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<FuelType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}

