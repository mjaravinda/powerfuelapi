﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Models;

namespace PowerFuel.DA
{
    public class MainDbContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=JANAK\MSSQL;Database=PowerFuelDb;Trusted_Connection=True;");  
            optionsBuilder.UseSqlServer(@"Data Source=SQL5080.site4now.net;Initial Catalog=db_a66697_powerfuel;User Id=db_a66697_powerfuel_admin;Password=Janak@001");  
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<FuelStation> FuelStations { get; set; }
       // public virtual DbSet<FuelStationDailyPlan> FuelStationDailyPlans { get; set; }
        public virtual DbSet<FuelStationLog> FuelStationLogs { get; set; }
        public virtual DbSet<FuelStationCurrentStock> FuelStationCurrentStocks { get; set; }
        public virtual DbSet<FuelStationRequest> FuelStationRequests { get; set; }
        public virtual DbSet<FuelType> FuelTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<VehicleType> VehicleTypes { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<VehicleQuataRequest> VehicleQuataRequests { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
    }
}