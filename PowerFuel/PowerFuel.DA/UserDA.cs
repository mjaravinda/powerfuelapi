﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class UserDA : IGenaricDA<User>,IUserDA
    {
        public async Task<ActionResult<User>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var user = await db.Users.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (user != null)
                    {
                        user.State = Common.State.Deleted;
                        return new ActionResult<User> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<User> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<User> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var users = await db.Users.Where(w => w.State == Common.State.Active).ToListAsync();
                    return users;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<User?> GetByEmailOrPhoneNumber(string content)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var user = await db.Users.Where(w => w.Email == content || w.ContactNo == content).FirstOrDefaultAsync();
                    return user;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<User?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var user = await db.Users.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return user;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<User>> Save(User t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        t.State = Common.State.Active;
                        t.Created = DateTime.UtcNow;
                        t.LastUpdated = DateTime.UtcNow;
                        db.Users.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var user = await db.Users.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (user != null)
                        {
                            user.FirstName = t.FirstName;
                            user.LastName = t.LastName;
                            user.Email = t.Email;
                            user.ContactNo = t.ContactNo;
                            user.FuelStationId= t.FuelStationId;
                            user.LastUpdated = DateTime.UtcNow;
                            user.UserTypeId = t.UserTypeId;                       
                            if (!string.IsNullOrEmpty(user.Password))
                            {
                                user.Password = t.Password;
                            }
                            await db.SaveChangesAsync();
                            t.Id = user.Id;
                        }                   
                    }

                    return new ActionResult<User> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<User> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}
