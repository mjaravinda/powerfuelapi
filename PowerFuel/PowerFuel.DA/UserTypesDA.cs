﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class UserTypesDA : IGenaricDA<UserType>
    {
        public async Task<ActionResult<UserType>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var userType = await db.UserTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (userType != null)
                    {
                        db.UserTypes.Remove(userType);
                        await db.SaveChangesAsync();
                        return new ActionResult<UserType> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<UserType> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<UserType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<UserType>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var userTypes = await db.UserTypes.ToListAsync();
                    return userTypes;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<UserType?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var userType = await db.UserTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return userType;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<UserType>> Save(UserType t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        db.UserTypes.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var userType = await db.UserTypes.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (userType != null)
                        {
                            userType.Name = t.Name;
                            await db.SaveChangesAsync();
                        }
                    }
                    return new ActionResult<UserType> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<UserType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}

