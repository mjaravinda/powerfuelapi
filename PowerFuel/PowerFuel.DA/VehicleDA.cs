﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class VehicleDA : IGenaricDA<Vehicle>, IVehicleDA
    {
        public async Task<ActionResult<Vehicle>> ChangeStatus(long? VehicleId, VehicleStatus status)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var v = db.Vehicles.FirstOrDefault(w => w.Id == VehicleId);                   
                    if (v.VehicleStatus != status) 
                    {
                        v.LastUpdated = DateTime.UtcNow;
                        db.Notifications.Add(new  Notification
                        {
                            Id= 0,
                            Created =DateTime.UtcNow,
                            CustomerId = v.CustomerId,
                            VehicleId = v.Id,
                            LastUpdated =DateTime.UtcNow,
                            NotificationStatus = NotificationStatus.New,
                            State = State.Active,
                            Description =$"Vehicle Status changed to {status.ToString()} "

                        });
                        v.VehicleStatus = status;

                        await  db.SaveChangesAsync();
                       
                    }

                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<Vehicle>> ChangeStatus(long?[] VehicleId, VehicleStatus status)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vList = db.Vehicles.Where(w => VehicleId.Contains(w.Id)).ToList();
                    foreach (var v in vList)
                    {
                        if (v.VehicleStatus != status)
                        {
                            v.LastUpdated = DateTime.UtcNow;
                            db.Notifications.Add(new Notification
                            {
                                Id = 0,
                                Created = DateTime.UtcNow,
                                CustomerId = v.CustomerId,
                                VehicleId = v.Id,
                                LastUpdated = DateTime.UtcNow,
                                NotificationStatus = NotificationStatus.New,
                                State = State.Active,
                                Description = $"Vehicle Status changed to {status.ToString()} "

                            });
                            v.VehicleStatus = status;

                           

                        }
                    }
                    await db.SaveChangesAsync();

                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<Vehicle>> Delete(long? id)
        {

            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicle = await db.Vehicles.Where(w => w.Id == id).FirstOrDefaultAsync();
                    vehicle.State = Common.State.Deleted;
                    return new ActionResult<Vehicle> { Status = Common.ActionStatus.Success };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<Vehicle> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<Vehicle>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext()) 
                {
                    var vehicles =await db.Vehicles.Where(w=> w.State == State.Active).Include(i=> i.VehicleType).Include(i=> i.Customer).Include(i=> i.FuelType).ToListAsync();

                    return vehicles;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<Vehicle>> GetAll(long? customerId)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicles = await db.Vehicles.Where(w => w.State == State.Active && w.CustomerId == customerId).Include(i => i.VehicleType).Include(i => i.Customer).Include(i => i.FuelType).ToListAsync();

                    return vehicles;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public Task<Vehicle?> GetById(long? id)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResult<Vehicle>> Save(Vehicle t)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        t.State = State.Active;
                        t.Created = DateTime.UtcNow;
                        t.LastUpdated = DateTime.UtcNow;
                        db.Vehicles.Add(t);

                        db.Notifications.Add(new Notification
                        {
                            Id = 0,
                            Created = DateTime.UtcNow,
                            CustomerId = t.CustomerId,
                            Vehicle = t,
                            LastUpdated = DateTime.UtcNow,
                            NotificationStatus = NotificationStatus.New,
                            State = State.Active,
                            Description = $"Vehicle has been created"

                        });
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var v = db.Vehicles.FirstOrDefault(f => f.Id == t.Id);
                        if (v.ChassyNo != t.ChassyNo || v.PlateNo != t.PlateNo)
                        {
                            v.VehicleStatus = VehicleStatus.Created;
                            db.Notifications.Add(new Notification
                            {
                                Id = 0,
                                Created = DateTime.UtcNow,
                                CustomerId = t.CustomerId,
                                Vehicle = t,
                                LastUpdated = DateTime.UtcNow,
                                NotificationStatus = NotificationStatus.New,
                                State = State.Active,
                                Description = $"Vehicle Status changed to created due to plateNo, chassy no modification"

                            });


                            var requests = db.VehicleQuataRequests.Where(w => w.VehicleId == v.Id && (w.RequestStatus == RequestStatus.Created || w.RequestStatus == RequestStatus.Confirmed)).ToList();
                            if (requests != null && requests.Any())
                            {
                                foreach (var r in requests)
                                {
                                    r.RequestStatus = RequestStatus.Canceled;

                                }

                                db.Notifications.Add(new Notification
                                {
                                    Id = 0,
                                    Created = DateTime.UtcNow,
                                    CustomerId = t.CustomerId,
                                    Vehicle = t,
                                    LastUpdated = DateTime.UtcNow,
                                    NotificationStatus = NotificationStatus.New,
                                    State = State.Active,
                                    Description = $"All pending quota requests due to plateNo, chassy no modification"

                                });
                            }
                        }
                        else if (v.VehicleTypeId != t.VehicleTypeId || v.FuelTypeId != t.FuelTypeId)
                        {

                            var requests = db.VehicleQuataRequests.Where(w => w.VehicleId == v.Id && (w.RequestStatus == RequestStatus.Created || w.RequestStatus == RequestStatus.Confirmed)).ToList();
                            if (requests != null && requests.Any())
                            {
                                foreach (var r in requests)
                                {
                                    r.RequestStatus = RequestStatus.Canceled;

                                }

                                db.Notifications.Add(new Notification
                                {
                                    Id = 0,
                                    Created = DateTime.UtcNow,
                                    CustomerId = t.CustomerId,
                                    Vehicle = t,
                                    LastUpdated = DateTime.UtcNow,
                                    NotificationStatus = NotificationStatus.New,
                                    State = State.Active,
                                    Description = @$"All pending quota requests due to VehicleType\Fuel Type modification"

                                });
                            }
                        }

                        v.PeriodQTA = t.PeriodQTA;
                        v.LastUpdated = t.LastUpdated;
                        v.ChassyNo = t.ChassyNo;
                        v.FuelTypeId = t.FuelTypeId;
                        v.PlateNo = t.PlateNo;
                        v.VehicleTypeId = t.VehicleTypeId;

                       await db.SaveChangesAsync();
                        
                    }

                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };

                }
            }
            catch (Exception e)
            {

                return new ActionResult<Vehicle> { Status = ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<ActionResult<Vehicle>> UpdateAlVehicles(List<Vehicle> vehicle)
        {
            try
            {
                using (var db =new MainDbContext())
                {
                    var ids =vehicle.Select(s => s.Id).ToArray();
                    var vehicles = db.Vehicles.Where(w=> ids.Contains(w.Id)).ToList();
                    if (vehicle != null && vehicle.Any())
                    {
                        foreach (var v in vehicles)
                        {
                            var updatedvehicle = vehicle.FirstOrDefault(w => w.Id == v.Id);
                            v.PeriodQTA = updatedvehicle?.PeriodQTA;    
                        }
                    }
                    await db.SaveChangesAsync();
                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };
                }

            }
            catch (Exception e)
            {

                return new ActionResult<Vehicle> { Status = ActionStatus.Error, Message = e.Message };
            }
            
        }
    }
}
