﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class VehicleRequestDA : IVehicleRequestDA
    {
        public async Task<List<VehicleQuataRequest>> GetFuelRequestList(DateTime start, DateTime end, long? fuelStationId, int? fuelTypeId)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var list = await db.VehicleQuataRequests.Where(f => f.State == Common.State.Active
                    && (f.RequestStatus == Common.RequestStatus.Confirmed || f.RequestStatus == Common.RequestStatus.Delivered)
                    && (start <= f.RequestedDateTime && f.RequestedDateTime <= end)
                    && f.FuelStationId == fuelStationId
                    && f.FuelTypeId == fuelTypeId).ToListAsync();


                    return list;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FuelStation> GetFuelStaion(VehicleQuataRequest request)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var f = await db.FuelStations.Where(f => f.Id == request.FuelStationId).FirstOrDefaultAsync();
                    return f;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Vehicle> GetVehicle(VehicleQuataRequest request)
        {

            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicle = await db.Vehicles.Where(f=> f.Id == request.VehicleId).Include(v=> v.VehicleType).FirstOrDefaultAsync();
                    return vehicle;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> IsQuataAvailable(VehicleQuataRequest request)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicle =await db.Vehicles.Where(w => w.Id == request.VehicleId).Include(i=> i.VehicleType).FirstOrDefaultAsync();
                    if (vehicle == null || vehicle.VehicleType == null)
                        return false;
                  
                    var monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                    var sunday = monday.AddDays(7).AddMinutes(-1);
                    DateTime? startDate = monday;
                    DateTime? endTime = sunday;

                    var requestedQty = db.VehicleQuataRequests.
                        Where(s => s.State == Common.State.Active 
                        && (s.RequestStatus == Common.RequestStatus.Confirmed || s.RequestStatus == Common.RequestStatus.Delivered) 
                        && startDate <= s.RequestedDateTime && s.RequestedDateTime <= endTime).Sum(s => s.Qty);
                    if ((requestedQty + request.Qty) > vehicle.VehicleType.PeriodAllocatedQTA)
                        return false;
                    else
                        return true;
                        
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<VehicleQuataRequest>> Save(VehicleQuataRequest request)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    if (request.Id == 0)
                    {
                        request.State = Common.State.Active;
                        db.VehicleQuataRequests.Add(request);
                        await db.SaveChangesAsync();
                      
                    }
                    else
                    {
                        var dbrequest = db.VehicleQuataRequests.FirstOrDefault(f=> f.Id == request.Id);
                        dbrequest.RequestStatus = request.RequestStatus;
                        dbrequest.Created = DateTime.UtcNow;
                        dbrequest.Qty = request.Qty;
                        await db.SaveChangesAsync();
                    }
                    return new ActionResult<VehicleQuataRequest> { Status = Common.ActionStatus.Success, Response = request };

                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
