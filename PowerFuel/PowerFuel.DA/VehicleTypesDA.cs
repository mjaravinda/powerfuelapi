﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class VehicleTypesDA : IGenaricDA<VehicleType>
    {
        public async Task<ActionResult<VehicleType>> Delete(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicleType = await db.VehicleTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    if (vehicleType != null)
                    {
                        db.VehicleTypes.Remove(vehicleType);
                        await db.SaveChangesAsync();
                        return new ActionResult<VehicleType> { Status = Common.ActionStatus.Success };
                    }
                    else
                    {
                        return new ActionResult<VehicleType> { Status = Common.ActionStatus.Error };
                    }
                }
            }
            catch (Exception e)
            {

                return new ActionResult<VehicleType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public async Task<IEnumerable<VehicleType>> GetAll()
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicleTypes = await db.VehicleTypes.ToListAsync();
                    return vehicleTypes;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<VehicleType?> GetById(long? id)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicleType = await db.VehicleTypes.Where(w => w.Id == id).FirstOrDefaultAsync();
                    return vehicleType;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<VehicleType>> Save(VehicleType t)
        {
            try
            {

                using (var db = new MainDbContext())
                {
                    if (t.Id == 0)
                    {
                        db.VehicleTypes.Add(t);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var vehicleType = await db.VehicleTypes.Where(w => w.Id == t.Id).FirstOrDefaultAsync();
                        if (vehicleType != null)
                        {
                            vehicleType.Name = t.Name;
                            vehicleType.PeriodAllocatedQTA = t.PeriodAllocatedQTA;
                            vehicleType.Period = t.Period;
                            vehicleType.NoOfPeriods = t.NoOfPeriods;
                            await db.SaveChangesAsync();
                        }
                      

                    }
                    return new ActionResult<VehicleType> { Status = Common.ActionStatus.Success, Response = t };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<VehicleType> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }
    }
}
