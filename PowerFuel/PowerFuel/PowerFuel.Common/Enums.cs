﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common
{
    public enum State
    {
        Deleted = -1,
        InActive =0,
        Active =1
    }

    public enum StockStatus
    { 
        In =1,
        Out =2
    }

    public enum Period
    {
        Day =1, Week =2, Month =3, Year =4
    }

    public enum VehicleStatus
    { 
        Created =1,
        Confirmed=2,
        Terminated =3
    }
    public enum RequestStatus
    {
        Created =1,
        Confirmed=2,
        Delivered=3,
        Canceled =4
    }

    public enum NotificationStatus
    {
        New =1, 
        Read =2,
        Close =3
    }

    public enum ActionStatus
    {
        Success =1,
        Error=2
    }
}
