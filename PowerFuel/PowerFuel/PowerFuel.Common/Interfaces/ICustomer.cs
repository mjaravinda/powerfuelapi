﻿using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Interfaces
{
    public interface ICustomerDA
    {


        Task<Customer?> GetByEmailOrPhoneNumber(string content);
       
    }

    public interface ICustomerBL
    {
        Task<ActionResult<Customer>> Login(LoginModel model);
       
    }
}
