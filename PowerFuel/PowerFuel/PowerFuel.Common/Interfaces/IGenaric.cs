﻿using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Interfaces
{
    public interface IGenaricDA<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T?> GetById(long? id);
        Task<ActionResult<T>> Save(T t);
        Task<ActionResult<T>> Delete(long? id);
    }

    public interface IGenaricBL<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(long? id);
        Task<ActionResult<T>> Save(T t);
        Task<ActionResult<T>> Delete(long? id);
    }


}
