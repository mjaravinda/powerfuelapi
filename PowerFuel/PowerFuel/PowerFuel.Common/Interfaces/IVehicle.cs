﻿using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Interfaces
{
    public interface IVehicleDA
    {
        Task<IEnumerable<Vehicle>> GetAll(long? customerId);
        Task<ActionResult<Vehicle>> ChangeStatus(long? VehicleId,VehicleStatus status);
        Task<ActionResult<Vehicle>> ChangeStatus(long?[] VehicleId,VehicleStatus status);
        Task<ActionResult<Vehicle>> UpdateAlVehicles(List<Vehicle> vehicle);
    }

    public interface IVehicleBL
    {
        Task<IEnumerable<Vehicle>> GetAll(long? customerId);
        Task<ActionResult<Vehicle>> ChangeStatus(long? VehicleId, VehicleStatus status);
        Task<ActionResult<Vehicle>> ChangeStatus(long?[] VehicleId, VehicleStatus status);
        Task<ActionResult<Vehicle>> UpdateAlVehicles(List<Vehicle> vehicle);
    }
}
