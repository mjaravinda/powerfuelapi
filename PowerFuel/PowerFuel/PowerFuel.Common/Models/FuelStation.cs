﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class FuelStation: GenaricModel
    {
        public string? Name { get; set; }
        public string? Address { get; set; }
        public int? DistrictId { get; set; }
        public string? ContactPerson { get; set; }
        public string? Email { get; set; }
        public int? DurationPerVerhicle { get; set; }
  
        [ForeignKey("DistrictId")]
        public virtual District? District { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}
