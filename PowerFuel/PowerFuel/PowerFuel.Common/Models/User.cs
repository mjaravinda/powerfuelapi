﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class User:GenaricModel
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? ContactNo { get; set; }
        public string? Password { get; set; }
        public int? UserTypeId { get; set; }
        [ForeignKey("UserTypeId")]
        public virtual UserType? UserType { get; set; }

        public long? FuelStationId { get; set; }
        [ForeignKey("FuelStationId")]
        public virtual FuelStation? FuelStation { get; set; }

    }
}

