﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.Common.Models
{
    public class VehicleQuataRequest: GenaricModel
    {
        public long? FuelStationId { get; set; }
        [ForeignKey("FuelStationId")]
        public virtual FuelStation? FuelStation { get; set; }

        public long? VehicleId { get; set; }
        [ForeignKey("VehicleId")]
        public virtual Vehicle? Vehicle { get; set; }

        public int? FuelTypeId { get; set; }
        [ForeignKey("FuelTypeId")]
        public virtual FuelType? FuelType { get; set; }

        public float Qty { get; set; }
        public DateTime? RequestedDateTime { get; set; }

        public long? PumpedUserId { get; set; }
        [ForeignKey("PumpedUserId")]
        public virtual User? PumpedUser { get; set; }

        public RequestStatus? RequestStatus { get; set; }

    }
}
