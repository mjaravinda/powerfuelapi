﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PowerFuel.DA.Migrations
{
    public partial class AddTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FuelStationLogs_Users_UpdatedUserId",
                table: "FuelStationLogs");

            migrationBuilder.AlterColumn<long>(
                name: "UpdatedUserId",
                table: "FuelStationLogs",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.CreateTable(
                name: "FuelStationCurrentStocks",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FuelTypeId = table.Column<int>(type: "int", nullable: true),
                    FuelStationId = table.Column<long>(type: "bigint", nullable: true),
                    Qty = table.Column<float>(type: "real", nullable: false),
                    State = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FuelStationCurrentStocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FuelStationCurrentStocks_FuelStations_FuelStationId",
                        column: x => x.FuelStationId,
                        principalTable: "FuelStations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FuelStationCurrentStocks_FuelTypes_FuelTypeId",
                        column: x => x.FuelTypeId,
                        principalTable: "FuelTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FuelStationRequests",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FuelStationId = table.Column<long>(type: "bigint", nullable: true),
                    FuelTypeId = table.Column<int>(type: "int", nullable: true),
                    Qty = table.Column<float>(type: "real", nullable: false),
                    RequestedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReceivedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PumpedUserId = table.Column<long>(type: "bigint", nullable: true),
                    RequestStatus = table.Column<int>(type: "int", nullable: true),
                    State = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FuelStationRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FuelStationRequests_FuelStations_FuelStationId",
                        column: x => x.FuelStationId,
                        principalTable: "FuelStations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FuelStationRequests_FuelTypes_FuelTypeId",
                        column: x => x.FuelTypeId,
                        principalTable: "FuelTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FuelStationRequests_Users_PumpedUserId",
                        column: x => x.PumpedUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PlateNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ChassyNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FuelTypeId = table.Column<int>(type: "int", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    UsedQTA = table.Column<float>(type: "real", nullable: true),
                    PeriodQTA = table.Column<float>(type: "real", nullable: true),
                    CurrentPeriodStartTime = table.Column<float>(type: "real", nullable: true),
                    VehicleStatus = table.Column<int>(type: "int", nullable: true),
                    State = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Vehicles_FuelTypes_FuelTypeId",
                        column: x => x.FuelTypeId,
                        principalTable: "FuelTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VehicleId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    FuelStationId = table.Column<long>(type: "bigint", nullable: true),
                    NotificationStatus = table.Column<int>(type: "int", nullable: true),
                    State = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Notifications_FuelStations_FuelStationId",
                        column: x => x.FuelStationId,
                        principalTable: "FuelStations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Notifications_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "VehicleQuataRequests",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FuelStationId = table.Column<long>(type: "bigint", nullable: true),
                    VehicleId = table.Column<long>(type: "bigint", nullable: true),
                    FuelTypeId = table.Column<int>(type: "int", nullable: true),
                    Qty = table.Column<float>(type: "real", nullable: false),
                    RequestedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PumpedUserId = table.Column<long>(type: "bigint", nullable: true),
                    RequestStatus = table.Column<int>(type: "int", nullable: true),
                    State = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleQuataRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleQuataRequests_FuelStations_FuelStationId",
                        column: x => x.FuelStationId,
                        principalTable: "FuelStations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VehicleQuataRequests_FuelTypes_FuelTypeId",
                        column: x => x.FuelTypeId,
                        principalTable: "FuelTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VehicleQuataRequests_Users_PumpedUserId",
                        column: x => x.PumpedUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VehicleQuataRequests_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_FuelStationCurrentStocks_FuelStationId",
                table: "FuelStationCurrentStocks",
                column: "FuelStationId");

            migrationBuilder.CreateIndex(
                name: "IX_FuelStationCurrentStocks_FuelTypeId",
                table: "FuelStationCurrentStocks",
                column: "FuelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FuelStationRequests_FuelStationId",
                table: "FuelStationRequests",
                column: "FuelStationId");

            migrationBuilder.CreateIndex(
                name: "IX_FuelStationRequests_FuelTypeId",
                table: "FuelStationRequests",
                column: "FuelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FuelStationRequests_PumpedUserId",
                table: "FuelStationRequests",
                column: "PumpedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_CustomerId",
                table: "Notifications",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_FuelStationId",
                table: "Notifications",
                column: "FuelStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_VehicleId",
                table: "Notifications",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleQuataRequests_FuelStationId",
                table: "VehicleQuataRequests",
                column: "FuelStationId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleQuataRequests_FuelTypeId",
                table: "VehicleQuataRequests",
                column: "FuelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleQuataRequests_PumpedUserId",
                table: "VehicleQuataRequests",
                column: "PumpedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleQuataRequests_VehicleId",
                table: "VehicleQuataRequests",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_CustomerId",
                table: "Vehicles",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_FuelTypeId",
                table: "Vehicles",
                column: "FuelTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_FuelStationLogs_Users_UpdatedUserId",
                table: "FuelStationLogs",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FuelStationLogs_Users_UpdatedUserId",
                table: "FuelStationLogs");

            migrationBuilder.DropTable(
                name: "FuelStationCurrentStocks");

            migrationBuilder.DropTable(
                name: "FuelStationRequests");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "VehicleQuataRequests");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.AlterColumn<long>(
                name: "UpdatedUserId",
                table: "FuelStationLogs",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FuelStationLogs_Users_UpdatedUserId",
                table: "FuelStationLogs",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
