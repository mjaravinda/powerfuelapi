﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PowerFuel.DA.Migrations
{
    public partial class addSalt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Salt",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Salt",
                table: "Customers");
        }
    }
}
