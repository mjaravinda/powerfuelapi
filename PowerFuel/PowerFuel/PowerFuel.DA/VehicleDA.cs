﻿using Microsoft.EntityFrameworkCore;
using PowerFuel.Common;
using PowerFuel.Common.Interfaces;
using PowerFuel.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerFuel.DA
{
    public class VehicleDA : IGenaricDA<Vehicle>, IVehicleDA
    {
        public async Task<ActionResult<Vehicle>> ChangeStatus(long? VehicleId, VehicleStatus status)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var v = db.Vehicles.FirstOrDefault(w => w.Id == VehicleId);                   
                    if (v.VehicleStatus != status) 
                    {
                        v.LastUpdated = DateTime.UtcNow;
                        db.Notifications.Add(new  Notification
                        {
                            Id= 0,
                            Created =DateTime.UtcNow,
                            CustomerId = v.CustomerId,
                            VehicleId = v.Id,
                            LastUpdated =DateTime.UtcNow,
                            NotificationStatus = NotificationStatus.New,
                            State = State.Active,
                            Description =$"Vehicle Status changed to {status.ToString()} "

                        });
                        v.VehicleStatus = status;

                        await  db.SaveChangesAsync();
                       
                    }

                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<Vehicle>> ChangeStatus(long?[] VehicleId, VehicleStatus status)
        {
            try
            {
                using (var db = new MainDbContext())
                {
                    var vList = db.Vehicles.Where(w => VehicleId.Contains(w.Id)).ToList();
                    foreach (var v in vList)
                    {
                        if (v.VehicleStatus != status)
                        {
                            v.LastUpdated = DateTime.UtcNow;
                            db.Notifications.Add(new Notification
                            {
                                Id = 0,
                                Created = DateTime.UtcNow,
                                CustomerId = v.CustomerId,
                                VehicleId = v.Id,
                                LastUpdated = DateTime.UtcNow,
                                NotificationStatus = NotificationStatus.New,
                                State = State.Active,
                                Description = $"Vehicle Status changed to {status.ToString()} "

                            });
                            v.VehicleStatus = status;

                           

                        }
                    }
                    await db.SaveChangesAsync();

                    return new ActionResult<Vehicle> { Status = ActionStatus.Success };
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult<Vehicle>> Delete(long? id)
        {

            try
            {
                using (var db = new MainDbContext())
                {
                    var vehicle = await db.Vehicles.Where(w => w.Id == id).FirstOrDefaultAsync();
                    vehicle.State = Common.State.Deleted;
                    return new ActionResult<Vehicle> { Status = Common.ActionStatus.Success };
                }
            }
            catch (Exception e)
            {

                return new ActionResult<Vehicle> { Status = Common.ActionStatus.Error, Message = e.Message };
            }
        }

        public Task<IEnumerable<Vehicle>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Vehicle>> GetAll(long? customerId)
        {
            throw new NotImplementedException();
        }

        public Task<Vehicle?> GetById(long? id)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<Vehicle>> Save(Vehicle t)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<Vehicle>> UpdateAlVehicles(List<Vehicle> vehicle)
        {
            throw new NotImplementedException();
        }
    }
}
